#include <sys/io.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <exception>

#define EC_COMMAND_PORT         0x66
#define EC_DATA_PORT            0x62

#define IBF                     1
#define OBF                     0
#define EC_SC_READ_CMD          0x80

#define EC_REG_CPU_FAN_RPMS_HI  0xD0
#define EC_REG_CPU_FAN_RPMS_LO  0xD1
#define EC_REG_GPU_FAN_RPMS_HI  0xD2
#define EC_REG_GPU_FAN_RPMS_LO  0xD3

#define TEMP                    0x9E

/**
 * Set IO port input/output permissions
 * On success, zero is returned. On error, -1 is returned, and errno is
 * set appropriately.
 *
 * @return Status of Operation
 */
int ec_init() {
	if (ioperm(EC_DATA_PORT, 1, 1) != 0) {
		return EXIT_FAILURE;
	}

	if (ioperm(EC_COMMAND_PORT, 1, 1) != 0) {
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

/**
 * Wait on EC
 *
 * @param port The port for waiting
 * @param flag
 * @param value
 *
 * @return Status of Operation
 */
int ec_io_wait(const uint32_t port, const uint32_t flag, const char value) {
	uint8_t data = inb(port);
	int i = 0;

	while ((((data >> flag) & 0x1) != value) && (i++ < 100)) {
		data = inb(port);
	}

	if (i >= 100) {
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

/**
 * Read the Port informations
 *
 * @param port The port for waiting
 *
 * @return the Data
 */
uint8_t ec_io_read(const uint32_t port) {
	ec_io_wait(EC_COMMAND_PORT, IBF, 0);
	outb(EC_SC_READ_CMD, EC_COMMAND_PORT);

	ec_io_wait(EC_COMMAND_PORT, IBF, 0);
	outb(port, EC_DATA_PORT);

	ec_io_wait(EC_COMMAND_PORT, OBF, 1);
	uint8_t value = inb(EC_DATA_PORT);

	return value;
}

/**
 * Flush the EC
 */
void ec_flush() {
	while ((inb(EC_COMMAND_PORT) & 0x1) == 0x1) {
		inb(EC_DATA_PORT);
	}
}

/**
 * Read a byte from EC
 *
 * @return Returns the current byte
 */
int read_byte() {
	int i = 1000000;
	while ((inb(EC_COMMAND_PORT) & 1) == 0 && i > 0) {
		i -= 1;
	}

	if (i == 0) {
		return 0;
	} else {
		return inb(EC_DATA_PORT);
	}
}

/**
 * Send a command to the ec
 *
 * @param command the comamnd to send
 */
void send_command(int command) {
	int tt = 0;
	while ((inb(EC_COMMAND_PORT) & 2)) {
		tt++;
		if (tt > 30000) {
			break;
		}
	}

	outb(command, EC_COMMAND_PORT);
}

/**
 * Write data to ec
 *
 * @param data the data to write
 */
void write_data(int data) {
	while ((inb(EC_COMMAND_PORT) & 2));

	outb(data, EC_DATA_PORT);
}

/* Throw an exception if index is not valid
 *
 * @param index: 1 = CPU, 2 = GPU One, 3 = GPU Two
 */
void validate_index(int index) {
	if (index < 1 || index > 3) {
		throw std::invalid_argument("index must be 1, 2 or 3");
	}
}

/**
 * Read and return the Raw Fan duty of fan
 *
 * @param index the fan index
 *
 * @return the fan duty value
 */
int get_fan_duty(int index) {
	validate_index(index);

	ec_init();
	ec_flush();

	send_command(TEMP);
	write_data(index);

	read_byte();
	read_byte();
	int value = read_byte();

	return value;
}

/**
 * Read and returns the remote temp of the fan
 *
 * @param info the nodejs CallbackInfo
 *
 * @return the remote temp
 */
int get_remote_temp(int index) {
	validate_index(index);

	ec_init();
	ec_flush();

	send_command(TEMP);
	write_data(index);

	int value = read_byte();

	return value;
}

/**
 * Read and returns the local temp of the fan
 *
 * @param info the nodejs CallbackInfo
 *
 * @return the local temp
 */
int get_local_temp(int index) {
	validate_index(index);

	ec_init();
	ec_flush();

	send_command(TEMP);
	write_data(index);
	read_byte();

	return read_byte();
}

/**
 * Calculate the fan rpms
 *
 * @param raw_rpm_high The raw high rpm value of the fan
 * @param raw_rpm_low The raw low rpm value of the fan
 */
int calculate_fan_rpms(int raw_rpm_high, int raw_rpm_low) {
	int raw_rpm = (raw_rpm_high << 8) + raw_rpm_low;
	return raw_rpm > 0 ? (2156220 / raw_rpm) : 0;
}

/**
 * Read and returns the rpm of the fan
 *
 * @param info the nodejs CallbackInfo
 *
 * @return Returns the fan rpm
 */
int get_fan_rpm(int index) {
	validate_index(index);

	uint32_t hi_port, lo_port;
	switch (index) {
		case 1:
			hi_port = EC_REG_CPU_FAN_RPMS_HI;
			lo_port = EC_REG_CPU_FAN_RPMS_LO;
			break;
		case 2:
			hi_port = EC_REG_GPU_FAN_RPMS_HI;
			lo_port = EC_REG_GPU_FAN_RPMS_LO;
			break;
		case 3:
			hi_port = 0xD4;
			lo_port = 0xD5;
			break;
		default: throw std::logic_error("Invalid index even though it should've been validated by now"); break;
	}

	ec_init();
	ec_flush();

	int raw_rpm_hi = ec_io_read(hi_port);
	int raw_rpm_lo = ec_io_read(lo_port);

	return calculate_fan_rpms(raw_rpm_hi, raw_rpm_lo);
}

/**
 * Set the fan duty
 *
 * @param info the nodejs CallbackInfo
 *
 * @return Returns a bool to indicate was the operation sucessfully or not
 */
void set_fan_duty(int index, int fan_duty) {
	validate_index(index);

	if (fan_duty < 0 || fan_duty > 255) {
		throw std::invalid_argument("fan_duty must be 0 to 255");
	}

	ec_init();
	send_command(0x99);

	write_data(index);
	write_data(fan_duty);
}

/**
 * Set the fan duty on auto modus
 *
 * @param index 1 = CPU, 2 = GPU One, 3 = GPU Two, 5 = ALL
 */
void set_auto_fan_duty(int index) {
	if ((index < 1 || index > 3) && index != 5) {
		throw std::invalid_argument("index must be 1, 2, 3 or 5");
	}

	ec_init();

	send_command(0x99);
	write_data(0xff);

	switch (index) {
		case 1:
			write_data(0x01);
			break;
		case 2:
			write_data(0x02);
			break;
		case 3:
			write_data(0x03);
			break;
		case 5:
			write_data(0xff);
			write_data(0xff);
			break;
		default: throw std::logic_error("Invalid index even though it should've been validated by now"); break;
	}
}

void print_help() {
	std::cerr
		<< "fan_control -- Read and control fan status" << std::endl << std::endl
		<< "* fan_control duty <index> [value]" << std::endl
		<< "* fan_control rpm <index>" << std::endl
		<< "* fan_control temp <index>" << std::endl
		<< std::endl
		<< "index: fan to query/control: 1: CPU, 2: GPU one, 3: GPU two" << std::endl
		<< "value: 0-255 or \"auto\"" << std::endl
		;
}

int main(int argc, char *argv[]) {
	if (geteuid() != 0) {
		std::cerr << "This program should be run as root" << std::endl;
		return 1;
	}

	if (argc <= 2 || strcmp("--help", argv[1]) == 0 || strcmp("-h", argv[1]) == 0) {
		print_help();
		return 1;
	}

	if (strcmp("rpm", argv[1]) == 0) {
		// Fan number, see validate_index and set_auto_fan_duty docstrings
		int index = atoi(argv[2]);

		if (argc > 3) {
			std::cerr << "RPM is not settable" << std::endl;
			return 1;
		} else {
			std::cout << get_fan_rpm(index) << " RPM" << std::endl;
		}
		return 0;
	}

	if (strcmp("duty", argv[1]) == 0) {
		// Fan number, see validate_index and set_auto_fan_duty docstrings
		int index = atoi(argv[2]);

		if (argc > 3) {
			if (strcmp("auto", argv[3]) == 0) {
				set_auto_fan_duty(index);
			} else {
				int value = atoi(argv[3]);
				set_fan_duty(index, value);
			}
		} else {
			int duty = get_fan_duty(index);
			std::cout << duty << "/255 (=" << ((100 * duty) / 255) << "%)" << std::endl;
		}
		return 0;
	}

	if (strcmp("temp", argv[1]) == 0) {
		// Fan number, see validate_index and set_auto_fan_duty docstrings
		int index = atoi(argv[2]);

		if (argc > 3) {
			std::cerr << "Temperature is not settable" << std::endl;
			return 1;
		} else {
			int local = get_local_temp(index);
			int remote = get_remote_temp(index);
			std::cout
				<< "Local: " << local << "°C" << std::endl
				<< "Remote: " << remote << "°C" << std::endl;
		}
		return 0;
	}

	print_help();
	return 1;
}
